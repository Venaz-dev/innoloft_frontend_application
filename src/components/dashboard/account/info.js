import React, { useState } from "react";
import PasswordStrengthBar from "react-password-strength-bar";

const Info = () => {
  const [userInfo, setInfo] = useState({
    first_name: "",
    last_name: "",
    street: "",
    house_no: "",
    postal_code: "",
    country:""
  });
  const handleChange = (event) => {
    const { name, value } = event.target;
    setInfo({
      ...userInfo,
      [name]: value,
    });
  };
  return (
    <div className="account-settings">
      <form autoComplete="off">
        <div className="form-group">
          <label>First Name</label>
          <input
            type="text"
            placeholder="First Name"
            className="form-input"
            name="first_name"
            onChange={handleChange}
            value={userInfo.email}
            autoComplete="off"
          />
        </div>
        <div className="form-group">
          <label>Last Name</label>
          <input
            type="text"
            placeholder="Last Name"
            className="form-input"
            name="last_name"
            onChange={handleChange}
            value={userInfo.email}
            autoComplete="off"
          />
        </div>
        <div className="form-group">
          <label>Street</label>
          <input
            type="text"
            placeholder="Adress Street"
            className="form-input"
            name="street"
            onChange={handleChange}
            value={userInfo.email}
            autoComplete="off"
          />
        </div>
        <div className="form-col">
          <div className="form-group">
            <label>House No.</label>
            <input
              type="text"
              placeholder="Adress Street"
              className="form-input"
              name="house_no"
              onChange={handleChange}
              value={userInfo.email}
              autoComplete="off"
            />
          </div>
          <div className="form-group">
            <label>Postal Code</label>
            <input
              type="text"
              placeholder="Adress Street"
              className="form-input"
              name="postal_code"
              onChange={handleChange}
              value={userInfo.email}
              autoComplete="off"
            />
          </div>
        </div>
        <div className="form-group">
            <label>Country</label>
            <select
              placeholder="Adress Street"
              className="form-input"
              name="country"
              onChange={handleChange}
              value={userInfo.email}
              autoComplete="off"
            >
                <option value="">Select Country</option>
                <option value="germany">Germany</option>
                <option value="austria">Austria</option>
                <option value="switzerland">Switzerland</option>
            </select>
          </div>
      </form>
      <div className="btn-holder">
        <button className="btn btn-primary">Update</button>
      </div>
    </div>
  );
};

export default Info;
