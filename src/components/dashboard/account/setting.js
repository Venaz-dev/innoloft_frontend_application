import React, { useState } from "react";
import PasswordStrengthBar from "react-password-strength-bar";

const Setting = () => {
  const [userAccount, setAccount] = useState({
    email: "",
    password: "",
    passwordConfirm: "",
    strength: "",
  });
  const handleChange = (event) => {
    const { name, value } = event.target;
    setAccount({
      ...userAccount,
      [name]: value,
    });
  };
  return (
    <div className="account-settings">
      <form autoComplete="off">
        <div className="form-group">
          <label>E-mail</label>
          <input
            type="email"
            placeholder="E-mail address"
            className="form-input"
            name="email"
            onChange={handleChange}
            value={userAccount.email}
            autoComplete="off"
          />
        </div>
        <div className="form-group">
          <label>Password</label>
          <input
            type="password"
            placeholder="Enter Password"
            className="form-input"
            name="password"
            onChange={handleChange}
            value={userAccount.password}
            autoComplete="off"
          />
        </div>
        <div
          className={
            userAccount.password != "" ? "password-strength" : "d-none"
          }
        >
          <PasswordStrengthBar password={userAccount.password} />
        </div>
        <div className="form-group">
          <label>Confirm Password</label>
          <input
            type="password"
            placeholder="Confirm Password"
            className="form-input"
            name="passwordConfirm"
            onChange={handleChange}
            value={userAccount.passwordConfirm}
            autoComplete="off"
          />
        </div>
      </form>
      <div className="btn-holder">
        <button className="btn btn-primary">Update</button>
      </div>
    </div>
  );
};

export default Setting;
