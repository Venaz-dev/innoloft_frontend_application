import React, { useState } from "react";
import { AccountTopBar } from "../../shared/sidemenu";
import Setting from "./account/setting"
import Info from "./account/info"

const Account = () => {
  const [active, setActive] = useState("account_settings");

 

  

  const handleActive = (active) => {
    setActive(active);
  };
  return (
    <div className="account">
      <div className="top-bar">
        {AccountTopBar.map((item) => (
          <span
            key={item.id}
            className={item.action != active ? "active" : null}
            onClick={() => handleActive(item.action)}
          >
            {item.title}
          </span>
        ))}
      </div>
      {
        active === "account_settings" ? <Setting /> : <Info/>
      }
      
    </div>
  );
};

export default Account;
