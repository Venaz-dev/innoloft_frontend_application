import React from "react";
import Message from "../../assets/envelope.svg";
import Notification from "../../assets/notification.svg";
import Language from "../../assets/world.svg";

const Nav = () => {
  return (
    <nav className="navbar">
      <div className="logo">Inno</div>
      <div className="icons">
        <div>
          <img src={Language} />
          EN
        </div>
        <div>
          <img src={Message} />
        </div>
        <div>
          <img src={Notification} />
        </div>
      </div>
    </nav>
  );
};

export default Nav;
