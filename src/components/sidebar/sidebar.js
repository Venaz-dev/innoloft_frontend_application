import React from "react";
import { sidebarItems } from "../../shared/sidemenu";
import home from "../../assets/home.svg"


const Sidebar = ({handleAction, active}) => {
  return (
    <div className="sidebar">
      {sidebarItems.map((item) => (
        <div key={item.id} className="side-item" onClick={() => handleAction(item.action)}>
          <img src={item.icon} />
          <p className={active === item.action && "active"}>{item.title}</p>
        </div>
      ))}
    </div>
  );
};

export default Sidebar;
