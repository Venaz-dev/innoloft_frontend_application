import React from "react";
import Nav from "./header/nav";

const Layout = ({ children }) => {
  return (
    <div className="layout">
      <Nav />
      <div className="content">{children}</div>
    </div>
  );
};

export default Layout;
