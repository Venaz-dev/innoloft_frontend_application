import logo from "./logo.svg";
import "./styles/app.scss";
import "./components/layout";
import Layout from "./components/layout";
import Sidebar from "./components/sidebar/sidebar";
import Account from "./components/dashboard/account"
import { useState } from "react";

const App = () => {

  const [action, setAction] = useState("account")

  const handleAction = (action) => {
    setAction(action)
  }
  return (
    <Layout>
    
      <div className="dashboard">
        <div className="sidebar">
        <Sidebar handleAction={handleAction} active={action}/>
        </div>
        <div className="action">
          {
            action === "account" && <Account />
          }
        </div>
      </div>
    </Layout>
  );
};

export default App;
