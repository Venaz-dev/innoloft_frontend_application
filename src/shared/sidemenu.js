import home from "../assets/home.svg";
import account from "../assets/account.svg";
import company from "../assets/company.svg";
import settings from "../assets/settings.svg";
import news from "../assets/news.svg";
import analytics from "../assets/analytics.svg";
const sidebarItems = [
  {
    icon: home,
    title: "Home",
    id: 1,
    action: "home",
  },
  {
    icon: account,
    title: "My Account",
    id: 2,
    action: "account",
  },
  {
    icon: company,
    title: "My Company",
    id: 3,
    action: "company",
  },
  {
    icon: settings,
    title: "My Settings",
    id: 4,
    action: "settings",
  },
  {
    icon: news,
    title: "News",
    id: 5,
    action: "news",
  },
  {
    icon: analytics,
    title: "Analytics",
    id: 6,
    action: "analytics",
  },
];

export { sidebarItems };

const AccountTopBar = [
    {
        title: "Account Settings",
        id: 1,
        action: "account_settings"
    },
    {
        title: "User Information",
        id: 2,
        action: "user_information"
    }
]

export { AccountTopBar }